module PositionHelper
  def position_author_menu(selected)
    opts = Position
      .pluck(:user_id)
      .uniq
      .map{ |id| User.find_by(id: id) }
      .compact
      .sort_by{ |u| u.name }
      .map{ |u| [u.name, u.id] }
    opts.unshift [t("position.author"), ""]
    options_for_select(opts, selected)
  end

  def position_order_menu(selected)
    opts = [
      [t("updated"),          "updated"],
      [t("created"),          "created"],
      [t("position.opening"), "opening"],
    ]
    options_for_select(opts, selected)
  end
end
