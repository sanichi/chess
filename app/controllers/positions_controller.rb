class PositionsController < ApplicationController
  load_and_authorize_resource except: ["review", "retire"]
  authorize_resource only: ["review", "retire"]

  def index
    @positions = Position.search(@positions, params, positions_path, per_page: 10)
  end

  def select
    per_page = helpers.tests_per_page(params[:per_page])
    params[:user_id] = current_user.id
    @positions = Position.select(@positions, params, select_positions_path, per_page: per_page)
    remember_reviews(@positions)
  end

  def review
    update_test(params[:pid].to_i, params[:quality].to_i)
    @title = reviews_title
    @position = get_next_review
    redirect_to select_positions_path unless @position
  end

  def retire
    forget_reviews
    redirect_to home_path
  end

  def create
    @position.user = current_user
    if @position.save
      redirect_to @position
    else
      failure @position
      render :new
    end
  end

  def update
    if @position.update(resource_params)
      redirect_to @position
    else
      failure @position
      render :edit
    end
  end

  def destroy
    @position.destroy
    redirect_to positions_path
  end

  def copy
    @position = @position.dup
    render :new
  end

  private

  def resource_params
    params.require(:position).permit(:opening, :pgn, :svar, :ssvar, :var)
  end

  def remember_reviews(pager)
    session[:reviews] = pager.matches.pluck(:id)
    session[:rvindex] = 0
    session[:repeats] = []
  end

  def forget_reviews
    session[:reviews] = nil
    session[:rvindex] = nil
    session[:repeats] = nil
  end

  def get_next_review
    pids, rids, i = okay
    return unless i
    position = nil
    if i < pids.length
      position = Position.find_by(id: pids[i])
      session[:rvindex] += 1
    elsif !rids.empty?
      position = Position.find_by(id: rids.first)
      session[:repeats].rotate!
    end
    position
  end

  def update_test(pid, q)
    pids, rids, i = okay
    return unless i
    return unless pids.include?(pid)
    return unless Review::QUALITY.include?(q)
    test = Review.find_or_create_by(position_id: pid, user_id: current_user.id)
    test.step(q)
    test.save!
    if q < 3
      session[:repeats].push(pid) unless rids.include?(pid)
    else
      session[:repeats].delete(pid) if rids.include?(pid)
    end
  end

  def reviews_title
    pids, rids, i = okay
    return I18n.t("review.review") unless i
    if i < pids.length
      "%d of %d Review%s" % [session[:rvindex] + 1, pids.length, pids.length > 1 ? "s" : ""]
    elsif !rids.empty?
      "%d Repeat%s" % [rids.length, rids.length > 1 ? "s" : ""]
    else
      I18n.t("review.review")
    end
  end

  def okay
    vals = []
    return vals unless session[:reviews].is_a?(Array) && !session[:reviews].empty?
    vals.push session[:reviews]
    return vals unless session[:repeats].is_a?(Array)
    vals.push session[:repeats]
    return vals unless session[:rvindex].is_a?(Integer) && session[:rvindex] >= 0
    vals.push session[:rvindex]
    vals
  end
end
