module Model exposing (Model, State, flipOrientation, init, position, state, toggleNotation, updateIndex)

import Array exposing (Array)
import Colour exposing (Colour)
import Mark exposing (Mark)
import Position exposing (Position)
import Preferences exposing (Preferences)
import Scheme exposing (Scheme)


type alias Model =
    { bid : Int
    , positions : Array Position
    , index : Int
    , reset : Int
    , orientation : Colour
    , notation : Bool
    , pointer : Bool
    , marks : List Mark
    , scheme : Scheme
    }


init : Preferences -> Model
init preferences =
    let
        bid =
            preferences.bid

        positions =
            preferences.fens
                |> List.map Position.fromFen
                |> Array.fromList

        index =
            if preferences.index < 0 || preferences.index >= Array.length positions then
                0

            else
                preferences.index

        orientation =
            Colour.fromString preferences.orientation

        notation =
            preferences.notation

        pointer =
            preferences.pointer

        marks =
            Mark.fromList preferences.marks

        scheme =
            Scheme.fromString preferences.scheme
    in
    Model bid positions index index orientation notation pointer marks scheme


position : Model -> Position
position model =
    model.positions
        |> Array.get model.index
        |> Maybe.withDefault (Position.fromFen Position.defaultFen)


flipOrientation : Model -> Model
flipOrientation model =
    { model | orientation = Colour.not model.orientation }


toggleNotation : Model -> Model
toggleNotation model =
    { model | notation = not model.notation }


updateIndex : Model -> String -> Model
updateIndex model btn =
    let
        index =
            case btn of
                "n" ->
                    if model.index + 1 < Array.length model.positions then
                        model.index + 1

                    else
                        model.index

                "p" ->
                    if model.index > 0 then
                        model.index - 1

                    else
                        model.index

                "r" ->
                    model.reset

                "s" ->
                    0

                _ ->
                    case String.toInt btn of
                        Just ind ->
                            if ind >= 0 && ind < Array.length model.positions then
                                ind

                            else
                                model.index

                        Nothing ->
                            model.index
    in
    { model | index = index }


type alias State =
    { bid : Int
    , index : Int
    , reset : Bool
    , start : Bool
    , end : Bool
    }


state : Model -> State
state model =
    let
        reset =
            model.index == model.reset

        start =
            model.index == 0

        end =
            model.index + 1 == Array.length model.positions
    in
    State model.bid model.index reset start end
