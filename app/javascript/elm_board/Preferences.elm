module Preferences exposing (Preferences, decode)

import Json.Decode as D exposing (Decoder, Value)
import Position


type alias Preferences =
    { bid : Int
    , fens : List String
    , index : Int
    , orientation : String
    , notation : Bool
    , pointer : Bool
    , marks : List String
    , scheme : String
    }


decode : Value -> Preferences
decode value =
    D.decodeValue flagsDecoder value |> Result.withDefault default


flagsDecoder : Decoder Preferences
flagsDecoder =
    D.map8 Preferences
        (D.field "bid" D.int |> withDefault default.bid)
        (D.field "fens" (D.list D.string) |> withDefault default.fens)
        (D.field "index" D.int |> withDefault default.index)
        (D.field "orientation" D.string |> withDefault default.orientation)
        (D.field "notation" D.bool |> withDefault default.notation)
        (D.field "pointer" D.bool |> withDefault default.pointer)
        (D.field "marks" (D.list D.string) |> withDefault default.marks)
        (D.field "scheme" D.string |> withDefault default.scheme)


default : Preferences
default =
    let
        fens =
            List.singleton Position.defaultFen
    in
    Preferences 0 fens 0 "white" False False [] "default"



-- from elm-community/json-extra


withDefault : a -> Decoder a -> Decoder a
withDefault fallback decoder =
    D.maybe decoder
        |> D.map (Maybe.withDefault fallback)
