module Messages exposing (..)


type Msg
    = FlipOrientation
    | ToggleNotation
    | UpdateIndex String
