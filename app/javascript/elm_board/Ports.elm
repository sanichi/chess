port module Ports exposing (..)

import Model


port error : String -> Cmd msg


port state : Model.State -> Cmd msg


port updateIndex : (String -> msg) -> Sub msg
