import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  static targets = [ "fen" ]

  copy() {
    navigator.clipboard.writeText(this.fenTarget.value);
    this.element.lastElementChild.innerText = "Copied";
  }
}
