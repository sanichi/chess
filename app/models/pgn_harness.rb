class PgnHarness
  attr_reader :error, :game

  def initialize(pgn)
    begin
      clean = self.class.clean(pgn)
      complete = self.class.complete(clean)
      @game = PGN.parse(complete, Encoding::UTF_8).first
      @error = I18n.t("position.errors.parse") unless @game
    rescue Whittle::ParseError => e
      @error = I18n.t("position.errors.parse")
    rescue StandardError => e
      @error = I18n.t("position.errors.unknown")
    end
  end

  def self.clean(pgn)
    return "" if pgn.blank?
    pgn = pgn.dup
    pgn.gsub!(/\r\n/, "\n")                            # convert line endings
    pgn.gsub!(/\r/, "\n")                              # convert line endings
    pgn.sub!(/\A[\n\s]+/, "")                          # remove leading space
    pgn.gsub!(/\s*\{(\s*\[[^\]\}]*\])*\s*\}\s*/, " ")  # remove time stamps
    pgn.gsub!(/(\d)\s+(\d+)\.\.\./, '\1 ')             # remove supurfluous black move numbers
    pgn.gsub!(/\][\n\s]*\[/, "]\n[")                   # headers always start a new line
    pgn.sub!(/\][\n\s]*([^\[\n\s])/, "]\n\n\\1")       # two new lines between last header and moves
    pgn.sub!(/[\n\s]{2,}\[.*\z/m, "\n")                # remove any games after the first
    pgn.sub!(/[\n\s]{2,}\z/, "\n")                     # remove trailing space
    pgn
  end

  # Complete some PGN that might be missing a result or a FEN.
  # This will allow it to be parsed by the Pgn class.
  def self.complete(pgn)
    return "" if pgn.blank?
    pgn = pgn.dup

    # Remove the unknown result symbol if there is one.
    pgn.sub!(/[\n\s]+\*[\n\s]*\z/, "\n")

    # If there's no result, provide a dummy one.
    unless pgn.match?(/[\n\s](1-0|0-1|1\/2-1\/2)[\n\s]*\z/)
      pgn.sub!(/[\n\s]*\z/, "\n1/2-1/2\n")
    end

    # If there's no FEN, assume the initial one.
    unless pgn.match?(/\[\s*FEN\s+\"[^"]+"\s*\]/)
      pgn = %Q{[FEN "%s"]\n%s} % [Fen::INITIAL, pgn]
    end

    pgn
  end

  def okay?
    !@error
  end

  # Some errors only become apparent after parsing has finished.
  # This check should only be called when needed (creation or
  # update but not accessing) to avoid repeating unecessary work.
  def check!
    return self unless okay?
    begin
      # first, check that getting the FENs doesn't raise any PGN errors
      begin
        @game.positions.each { |p| p.to_fen.to_s }
      rescue => e
        raise I18n.t("position.errors.fen")
      end

      # double check each FEN is legal
      @game.positions.each do |p|
        raise I18n.t("position.errors.fen") if Fen.new(p.to_fen.to_s).error?
      end

      # check the index makes sense
      raise I18n.t("position.errors.start") if index == 0 && @game.starting_position.to_fen.to_s == Fen::INITIAL
      raise I18n.t("position.errors.continuation") if index + 1 >= @game.positions.length
    rescue => e
      @error = e.message
    end
    self
  end

  def index
    return unless okay?
    return @index if @index
    @index = @game.moves.index { |m| m.annotation.present? && m.annotation.match?(/\$22[01]/) }
    if @index.nil?
      @index = 0
    else
      @index += 1
    end
    @index
  end

  def fen
    return unless okay?
    @game.positions[index].to_fen.to_s
  end

  def fens
    return unless okay?
    @game.positions.map { |p| p.to_fen.to_s }
  end

  def player
    return unless okay?
    @game.positions[index].player.to_s
  end

  def pgn
    return unless okay?
    lines = []

    unless @game.starting_position.to_fen.to_s == Fen::INITIAL
      lines.push '[FEN "%s"]' % @game.starting_position.to_fen
      lines.push ""
    end

    positions = @game.positions
    show_label = true
    moves = @game.moves.each_with_index.map do |move, i|
      position = positions[i]
      white = position.player == :white
      number = position.fullmove
      label = "#{number}.#{white ? '' : '..'}" if show_label
      notation = move.notation
      annotation = move.annotation ? " #{move.annotation}" : ""
      comment = comment(move.comment, top_level: true, html: false)
      show_label = !white
      [label, notation, annotation, comment].join
    end.join(" ")

    lines.push moves
    lines.join("\n")
  end

  def html(bid=0)
    return unless okay?
    moves = @game.moves
    positions= @game.positions
    show_label = true
    html = moves.each_with_index.map do |move, i|
      position = positions[i]
      white = position.player == :white
      number = position.fullmove
      label = "#{number}.#{white ? '' : '..'}" if show_label
      notation = move.notation
      annotation = decode(move.annotation)
      comment = comment(move.comment, top_level: true)
      show_label = !white || comment
      sep = white ? " " : "\n"
      %Q{%s<span id="em-%d-%d" class="em-%d elm-move">%s%s</span>%s%s} %
        [label, bid, i + 1, bid, notation, annotation, comment, sep]
    end.join("")
    ("\n" + html.rstrip + "\n").html_safe
  end

  private

  def comment(text, top_level: false, html: true)
    return if text.blank?
    raw_comment = text.squish.sub(/\A\{\s*/, "").sub(/\s*\}\z/, "")
    safe_comment = Loofah.fragment(raw_comment).scrub!(:prune).to_s
    return " #{safe_comment}" unless top_level
    html ? ' <span class="comment">{%s}</span>' % safe_comment : " {#{safe_comment}}"
  end

  def text(move, number, white, show_label)
    label = show_label ? "#{number}#{white ? '.' : '...'}" : ''
    notation = move.notation
    annotation = decode(move.annotation)
    comment = comment(move.comment)
    ["#{label}#{notation}#{annotation}#{comment} ", comment.present?]
  end

  def decode(nag)
    case nag
    when nil then nil
    when "" then nil
    when "$1"   then "!"
    when "$2"   then "?"
    when "$3"   then "‼"
    when "$4"   then "⁇"
    when "$5"   then "⁉"
    when "$6"   then "⁈"
    when "$7"   then "□"
    when "$10"  then "="
    when "$13"  then "∞"
    when "$14"  then "⩲"
    when "$15"  then "⩱"
    when "$16"  then "±"
    when "$17"  then "∓"
    when "$18"  then "+−"
    when "$19"  then "−+"
    when "$220" then "⬒"
    when "$221" then "⬓"
    else nag
    end
  end
end
