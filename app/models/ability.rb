class Ability
  include CanCan::Ability

  def initialize(user)
    if user.admin?
      can :manage, :all
    end

    unless user.guest?
      can :read, Position
      can [:create, :update, :destroy], Position, user_id: user.id
      can [:copy, :select, :retire, :review], Position
    end

    can :home, :page
  end
end
