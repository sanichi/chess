class Fen
  attr_reader :en_passant, :error, :pieces, :squares

  INITIAL = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
  EMPTY = "8/8/8/8/8/8/8/8 w - - 0 1"
  MAX = 90

  def initialize(input=INITIAL)
    @str = input.to_s.squish[0,MAX]
    parse
  end

  def error_position
    return "" unless @error && @index && @str.length >= @index
    @str[0,@index] + "^" + @str[@index..-1]
  end

  def to_s = @str
  def error? = !!@error
  def white_to_move? = !!@white_to_move
  def black_to_move? = !@white_to_move
  def white_castle_kside? = !!@castle_wk
  def white_castle_qside? = !!@castle_wq
  def black_castle_kside? = !!@castle_bk
  def black_castle_qside? = !!@castle_bq
  def half_moves = @half_moves || 0
  def full_moves = @full_moves || 1

  private

  def parse
    @error = nil
    @index = 0
    @pieces = Hash.new { |h, k| h[k] = [] }
    @squares = Hash.new
    begin
      pieces!
      space!
      to_move!
      space!
      castling!
      space!
      en_passant!
      space!
      half_moves!
      space!
      full_moves!
    rescue => e
      @error = e.message
    end
  end

  def pieces!
    file = 1
    rank = 8
    while rank > 1 || file < 9
      case @str[@index]
      when /([KQBNRPkqbnrp])/
        piece = $1
        square = to_sq(file, rank)
        @pieces[piece].push(square)
        @squares[square] = piece
        file += 1
      when /([1-8])/
        raise I18n.t("fen.errors.invalid") if @index > 0 && @str[@index - 1].match?(/[1-8]/)
        file += $1.to_i
      when "/"
        if file == 9
          file = 1
          rank -= 1
        else
          raise I18n.t("fen.errors.misplaced")
        end
      when nil
        raise I18n.t("fen.errors.premature")
      else
        raise I18n.t("fen.errors.invalid")
      end
      @index += 1
    end
  end

  def to_move!
    case @str[@index]
    when "w"
      @white_to_move = true
      @index += 1
    when "b"
      @white_to_move = false
      @index += 1
    when nil
      raise I18n.t("fen.errors.premature")
    else
      raise I18n.t("fen.errors.invalid")
    end
  end

  def castling!(start=true)
    case @str[@index]
    when "-"
      raise I18n.t("fen.errors.invalid") unless start
      @index += 1
    when " "
      raise I18n.t("fen.errors.invalid") if start
    when "K"
      raise I18n.t("fen.errors.invalid") if @castle_wk
      @castle_wk = true
      @index += 1
      castling!(false)
    when "Q"
      raise I18n.t("fen.errors.invalid") if @castle_wq
      @castle_wq = true
      @index += 1
      castling!(false)
    when "k"
      raise I18n.t("fen.errors.invalid") if @castle_bk
      @castle_bk = true
      @index += 1
      castling!(false)
    when "q"
      raise I18n.t("fen.errors.invalid") if @castle_bq
      @castle_bq = true
      @index += 1
      castling!(false)
    when nil
      raise I18n.t("fen.errors.premature")
    else
      raise I18n.t("fen.errors.invalid")
    end
  end

  def en_passant!(file=nil)
    case @str[@index]
    when "-"
      raise I18n.t("fen.errors.invalid") unless file.nil?
      @index += 1
    when /([a-h])/
      raise I18n.t("fen.errors.invalid") unless file.nil?
      @index += 1
      en_passant!($1)
    when /([36])/
      raise I18n.t("fen.errors.invalid") if file.nil?
      @en_passant = "#{file}#{$1}"
      @index += 1
    when nil
      raise I18n.t("fen.errors.premature")
    else
      raise I18n.t("fen.errors.invalid")
    end
  end

  def half_moves!(so_far="")
    case @str[@index]
    when "0"
      @index += 1
      if so_far.length == 0
        @half_moves = 0
      else
        half_moves!("#{so_far}0")
      end
    when /([1-9])/
      @index += 1
      half_moves!("#{so_far}#{$1}")
    when " "
      if so_far.length == 0
        raise I18n.t("fen.errors.invalid")
      else
        @half_moves = so_far.to_i
      end
    when nil
      raise I18n.t("fen.errors.premature")
    else
      raise I18n.t("fen.errors.invalid")
    end
  end

  def full_moves!(so_far="")
    case @str[@index]
    when /(\d)/
      if so_far.length == 0 && $1 == "0"
        raise I18n.t("fen.errors.invalid")
      else
        @index += 1
        full_moves!("#{so_far}#{$1}")
      end
    when nil
      if so_far.length == 0
        raise I18n.t("fen.errors.premature")
      else
        @full_moves = so_far.to_i
      end
    else
      raise I18n.t("fen.errors.invalid")
    end
  end

  def space!
    case @str[@index]
    when " "
      @index += 1
    when nil
      raise I18n.t("fen.errors.premature")
    else
      raise I18n.t("fen.errors.space")
    end
  end

  def to_sq(file, rank) = ('a'.ord + (file - 1)).chr + rank.to_s

  EXAMPLES = [
    "3rk2r/2R1bp2/q2p3p/4p1p1/pp6/3P3Q/PP2N2P/1KR5 w k - 0 1",
    "2B3K1/8/3N1p1p/6pk/5P1P/6P1/7r/5r2 w - - 0 1",
    "r4r1k/p1pb4/1p1p3p/3Pq3/2P1pp2/3BB3/PPQN1P1P/6RK w - - 0 1",
    "1r2qr1k/5pb1/5Rpp/1np1Pn2/2B1N3/6B1/1PP3PP/3Q1RK1 w - - 0 1",
    "r1bq1r2/4npbk/p1pp2pp/2p5/4P2Q/2NP1N2/PPP3PP/R1B2RK1 w - - 0 1",
    "q3rk2/1b2b1pp/p3Bn2/2p1N3/1p1r1B2/1P5P/P1Q2PP1/2R1R1K1 w - - 3 25",
    "8/8/7p/4k2P/4P1P1/2pK4/8/8 w - - 7 1",
    "r4r2/5q1k/6Rp/p3pP2/1pn4P/5PQ1/PPP3P1/2K4R w - - 1 1",
    "7K/P1p1p1p1/2P1P1Pk/6pP/3p2P1/1P6/3P4/8 w - - 0 1",
    "1RQ5/r4pbk/4p2p/8/2pP2p1/2P3P1/6NK/2q5 w - - 0 1",
    "8/6k1/4KpPp/5P2/8/8/8/8 w - - 0 1",
    "8/4P1r1/8/8/R5K1/8/8/7k w - - 0 1",
    "r2q1rk1/pbp2ppp/1n2p3/4PnN1/2pP4/P3B3/P1Q1BPPP/2R2RK1 b - - 4 15",
    "4r1k1/1p6/p2p4/P1pP1p2/R5n1/2N2KP1/1P3P1r/5R2 b - - 0 1",
    "8/p3k3/P4pp1/2p4p/7P/1P3KP1/5P2/8 w - - 0 52",
    "5Bk1/p1p3pp/1p5q/3P4/1PP1Qn2/P4P2/4p2P/4R2K b - - 0 28",
    "2r1r1k1/1p1q1ppp/3p1b2/p2P4/3Q4/5N2/PP2RPPP/4R1K1 w - - 0 18",
    "5k2/4R3/1pKB2b1/4p3/8/7P/6pp/8 w - - 0 1",
    "8/4b3/6p1/1p1kp3/1Pp3Pp/2P2K1P/3N4/8 b - - 2 64",
    "K7/8/1k6/1p6/8/2P5/1P6/8 w - - 0 1",
    "r4rnk/4qppn/p4b2/4pP2/1p2B1R1/8/PPP2B1Q/6RK w - - 0 1",
    "2r5/2r2pkp/pp4p1/2np4/4n3/1P2P3/P2NKPPP/2RRN3 b - - 0 1",
    "8/8/8/8/pk2K3/8/P4P2/8 w - - 0 1",
    "4n2k/1R4bp/4B1p1/2Qpp3/2p5/2P1P2P/1P1q2rB/R6K w - - 0 1",
    "rnb2rk1/pp3p1p/3q2pQ/3pN3/2pP4/2PB4/P1P2PPP/4RRK1 w - - 0 1",
    "6R1/8/8/5k2/2K4p/6p1/8/8 b - - 3 3",
    "8/2R5/6k1/1K6/6pp/8/8/8 w - - 0 1",
    "r1r3k1/1q3pPp/1N2n1P1/4p3/1bP1b3/1p2BB2/pK5Q/3R3R b - - 0 1",
    "4rrk1/4Qppp/p2P1n2/1p6/3p4/PB6/1PP3Pq/1K1RR3 w - - 0 1",
    "r4rk1/p1B2ppp/1p2p3/qP6/3n4/2nB4/1Q3PPP/R4RK1 b - - 0 1",
    "4rb2/1p1q1ppk/p1n1p2p/2p1Pn2/3PNR2/2P2K2/P2QBP2/6R1 w - - 0 1",
    "2k2brr/1ppbq1p1/2np4/1B2p1p1/Q3P3/2PPB1p1/P2N1PP1/RR4K1 w - - 0 1",
    "8/5N2/1p6/3k4/1PR2P2/8/1p2K3/8 w - - 0 1",
    "2k4r/1pp2ppp/p1p1bn2/4N3/1q1rP3/2N1Q3/PPP2PPP/R4RK1 w - - 0 1",
    "kr1r4/7p/2bqp1p1/p7/1Pp1PP1P/K1B1nNPB/PP5R/RN2Q3 b - - 0 1",
    "8/8/3k4/1p6/8/2P1P3/2P1K3/1b6 w - - 0 1",
    "8/kp6/4R3/1P2K3/8/3pp3/8/8 w - - 0 1",
    "r3b1rk/1p1qn1p1/p3p1np/3pPpBQ/3P4/3B1N1P/PP3PP1/2R2RK1 w - - 0 1",
    "5rk1/1b3ppp/p5q1/5Q2/3Bp2R/2P5/2P3PP/6K1 w - - 0 1",
    "4r3/1r2ppk1/p2p1np1/2qP4/PpP5/1P1Q4/2B1R1PP/5R1K w - - 0 1",
    "r4r1k/pbpq1p1B/1p2pp2/8/1b1P2Q1/8/PPP2PPP/2KR3R w - - 0 18",
    "6rk/5p1p/p4P2/1pbn2r1/3P3R/1PB4Q/PKP2R1P/6q1 b - - 0 1",
    "r2q4/1p3pk1/p2n1n1p/2pP2p1/P7/5QN1/1P3PPP/1B2R1K1 w - - 0 22",
    "4r3/5pk1/2Q2n2/3B2pp/2b2q2/P6P/6P1/R2R2K1 b - - 0 1",
    "8/1KP5/8/2p5/1pP5/p7/k7/1R3R2 w - - 0 1",
    "3R4/5p1p/4q3/6k1/2P5/2P2PN1/5K1P/8 w - - 0 1",
    "rnbqkb1r/ppp2ppp/8/3pP3/3Qn3/5N2/PPP2PPP/RNB1KB1R w KQkq d6 0 6",
  ]
end
