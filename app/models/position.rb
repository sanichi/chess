class Position < ApplicationRecord
  include Constrainable
  include Pageable

  MAX_OPENING = 25

  belongs_to :user
  has_many :reviews

  before_validation :normalize_attributes, :parse_pgn

  validates :fen, length: { maximum: Fen::MAX }, presence: true, uniqueness: { scope: :user, message: I18n.t("position.errors.dup") }
  validates :opening, length: { maximum: MAX_OPENING }, presence: true
  validates :var, :svar, :ssvar, length: { maximum: MAX_OPENING }, allow_nil: true

  def self.search(matches, params, path, opt={})
    matches = case params[:order]
    when "opening"
      matches.order(:opening, :var, :svar, :ssvar, :updated_at)
    when "created"
      matches.order(created_at: :desc)
    else
      matches.order(updated_at: :desc)
    end
    if sql = cross_constraint(params[:opening], %w{opening var svar ssvar})
      matches = matches.where(sql)
    end
    # matches = opening_author(matches, params) # for now, with only one author, no point
    paginate(matches, params, path, opt)
  end

  def self.select(matches, params, path, opt={})
    matches = opening_author(matches, params)
    if params[:type] == "new"
      pids = Review.where(user_id: params[:user_id]).pluck(:position_id)
      matches = matches.where.not(id: pids).order(:id)
    else
      matches = matches.joins(:reviews).where("reviews.user_id = ?", params[:user_id])
      case params[:type]
      when "day"
        matches = matches.where("reviews.due > ?", Time.now).where("reviews.due <= ?", Time.now + 1.day)
      when "days"
        matches = matches.where("reviews.due > ?", Time.now).where("reviews.due <= ?", Time.now + 3.day)
      when "week"
        matches = matches.where("reviews.due > ?", Time.now).where("reviews.due <= ?", Time.now + 7.day)
      else
        matches = matches.where("reviews.due <= ?", Time.now)
      end
    end
    paginate(matches, params, path, opt)
  end

  def harness
    @harness ||= PgnHarness.new(pgn)
  end

  def game
    @game ||= harness.game
  end

  def title
    [opening, var, svar, ssvar].compact.join(", ")
  end

  private

  def normalize_attributes
    %w(opening var svar ssvar).each do |a|
      if send(a).blank?
        send("#{a}=", nil)
      else
        send(a).squish!
      end
    end
  end

  def parse_pgn
    harness = PgnHarness.new(pgn).check!
    if harness.okay?
      self.pgn = harness.pgn
      self.fen = harness.fen
    else
      errors.add(:pgn, harness.error)
    end
  end

  def self.opening_author(matches, params)
    matches = matches.includes(:user)
    if sql = cross_constraint(params[:opening], %w{opening var svar ssvar})
      matches = matches.where(sql)
    end
    if (user_id = params[:author].to_i) > 0
      matches = matches.where(user_id: user_id)
    end
    matches
  end
end
