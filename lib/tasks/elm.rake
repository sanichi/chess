namespace :elm do
  def compile_esmify_minimize(name, args, main="Main")
    tmp = "_#{name}_elm.js"
    min = "../elm_#{name}.js"
    opt = args[:debug].present? ? "" : "--optimize"
    if system("elm make #{main}.elm #{opt} --output #{tmp}")
      File.open(min, "w") do |file|
        file.write(Terser.compile(esmify(File.read(tmp))))
      end
      File.delete(tmp)
      puts "minified to #{min}"
    end
  end

  # based on https://github.com/ChristophP/elm-esm/blob/master/src/index.js
  def esmify(text)
    commented = ""
    elmExports = nil
    filter = false
    text.each_line do |line|
      unless filter
        case line
        when /\A\(function\(scope\)/
          filter = 1
        when /\A['"]use strict['"];/
          filter = 1
        when /\Afunction _Platform_export/
          filter = "}\n"
        when /\Afunction _Platform_mergeExports/
          filter = "}\n"
        when /\A_Platform_export\((.*)\);\}\(this\)/
          filter = 1
          elmExports = $1
        end
      end
      commented += (filter ? "// -- " : "") + line
      if filter
        case filter
        when 1
          filter = false
        else
          filter = false if line == filter
        end
      end
    end

    unless elmExports
      puts "couldn't find elmExports"
      return text
    end

    commented += "\nexport const Elm = #{elmExports};\n"
    commented
  end

  # bin/rake elm:board
  # bin/rake elm:board\[o\]
  desc "make and modularize the JS file for Board"
  task :board, [:debug] do |task, args|
    Dir.chdir("app/javascript/elm_board") do
      compile_esmify_minimize("board", args, "Board")
    end
  end
end
