require 'rails_helper'

describe Fen do
  context "inital" do
    let(:initial) { Fen.new }

    it "error" do
      expect(initial).to_not be_error
    end

    it "string" do
      expect(initial.to_s).to eq Fen::INITIAL
    end

    it "to move" do
      expect(initial).to be_white_to_move
    end

    it "castling" do
      expect(initial.white_castle_kside?).to eq true
      expect(initial.white_castle_qside?).to eq true
      expect(initial.black_castle_kside?).to eq true
      expect(initial.black_castle_qside?).to eq true
    end

    it "half moves" do
      expect(initial.half_moves).to eq 0
    end

    it "full moves" do
      expect(initial.full_moves).to eq 1
    end

    it "pieces" do
      pieces = initial.pieces
      expect(pieces.length).to eq 12
      expect(pieces["r"]).to eq ["a8", "h8"]
      expect(pieces["n"]).to eq ["b8", "g8"]
      expect(pieces["b"]).to eq ["c8", "f8"]
      expect(pieces["q"]).to eq ["d8"]
      expect(pieces["k"]).to eq ["e8"]
      expect(pieces["p"]).to eq ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"]
      expect(pieces["P"]).to eq ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"]
      expect(pieces["R"]).to eq ["a1", "h1"]
      expect(pieces["N"]).to eq ["b1", "g1"]
      expect(pieces["B"]).to eq ["c1", "f1"]
      expect(pieces["Q"]).to eq ["d1"]
      expect(pieces["K"]).to eq ["e1"]
    end

    it "squares" do
      squares = initial.squares
      expect(squares.length).to eq 32
      expect(squares["a8"]).to eq "r"
      expect(squares["b8"]).to eq "n"
      expect(squares["c8"]).to eq "b"
      expect(squares["d8"]).to eq "q"
      expect(squares["e8"]).to eq "k"
      expect(squares["f8"]).to eq "b"
      expect(squares["g8"]).to eq "n"
      expect(squares["h8"]).to eq "r"
      expect(squares["a7"]).to eq "p"
      expect(squares["b7"]).to eq "p"
      expect(squares["c7"]).to eq "p"
      expect(squares["d7"]).to eq "p"
      expect(squares["e7"]).to eq "p"
      expect(squares["f7"]).to eq "p"
      expect(squares["g7"]).to eq "p"
      expect(squares["h8"]).to eq "r"
      expect(squares["a2"]).to eq "P"
      expect(squares["b2"]).to eq "P"
      expect(squares["c2"]).to eq "P"
      expect(squares["d2"]).to eq "P"
      expect(squares["e2"]).to eq "P"
      expect(squares["f2"]).to eq "P"
      expect(squares["g2"]).to eq "P"
      expect(squares["h2"]).to eq "P"
      expect(squares["a1"]).to eq "R"
      expect(squares["b1"]).to eq "N"
      expect(squares["c1"]).to eq "B"
      expect(squares["d1"]).to eq "Q"
      expect(squares["e1"]).to eq "K"
      expect(squares["f1"]).to eq "B"
      expect(squares["g1"]).to eq "N"
      expect(squares["h1"]).to eq "R"
    end
  end

  context "empty" do
    let(:empty) { Fen.new(Fen::EMPTY) }

    it "error" do
      expect(empty).to_not be_error
    end

    it "string" do
      expect(empty.to_s).to eq Fen::EMPTY
    end

    it "to move" do
      expect(empty).to be_white_to_move
    end

    it "castling" do
      expect(empty.white_castle_kside?).to eq false
      expect(empty.white_castle_qside?).to eq false
      expect(empty.black_castle_kside?).to eq false
      expect(empty.black_castle_qside?).to eq false
    end

    it "half moves" do
      expect(empty.half_moves).to eq 0
    end

    it "full moves" do
      expect(empty.full_moves).to eq 1
    end

    it "pieces" do
      pieces = empty.pieces
      expect(pieces.length).to eq 0
    end

    it "squares" do
      squares = empty.squares
      expect(squares.length).to eq 0
    end
  end

  context "random" do
    it "all OK" do
      en_passant = []
      half_moves = []
      full_moves = []
      Fen::EXAMPLES.each do |input|
        f = Fen.new(input)
        expect(f).to_not be_error
        expect(f.to_s).to eq input
        en_passant.push(f.en_passant) unless f.en_passant.nil?
        half_moves.push(f.half_moves) unless f.half_moves == 0
        full_moves.push(f.full_moves) unless f.full_moves == 1
      end
      expect(en_passant).to eq ["d6"]
      expect(half_moves).to eq [3, 7, 1, 4, 2, 3]
      expect(full_moves).to eq [25, 15, 52, 28, 18, 64, 3, 18, 22, 6]
    end
  end

  context "errors" do
    it "empty" do
      f = Fen.new(nil)
      expect(f).to be_error
      expect(f.error).to eq t("fen.errors.premature")
      expect(f.error_position).to eq "^"
    end

    it "piece" do
      f = Fen.new("rnbX")
      expect(f).to be_error
      expect(f.error).to eq t("fen.errors.invalid")
      expect(f.error_position).to eq "rnb^X"
    end

    it "multiple empty" do
      f = Fen.new("123")
      expect(f).to be_error
      expect(f.error).to eq t("fen.errors.invalid")
      expect(f.error_position).to eq "1^23"
    end

    it "slash" do
      f = Fen.new("rnbqkbn/")
      expect(f).to be_error
      expect(f.error).to eq t("fen.errors.misplaced")
      expect(f.error_position).to eq "rnbqkbn^/"
    end

    it "premature" do
      f = Fen.new("4k3/8/8/8/8/8/8/4K2")
      expect(f).to be_error
      expect(f.error).to eq t("fen.errors.premature")
      expect(f.error_position).to eq "4k3/8/8/8/8/8/8/4K2^"
    end

    it "mover" do
      f = Fen.new("4k3/8/8/8/8/8/8/4K3 W")
      expect(f).to be_error
      expect(f.error).to eq t("fen.errors.invalid")
      expect(f.error_position).to eq "4k3/8/8/8/8/8/8/4K3 ^W"
    end

    it "castling" do
      f = Fen.new("4k3/8/8/8/8/8/8/4K3 w KK")
      expect(f).to be_error
      expect(f.error).to eq t("fen.errors.invalid")
      expect(f.error_position).to eq "4k3/8/8/8/8/8/8/4K3 w K^K"
    end

    it "en passant" do
      f = Fen.new("4k3/8/8/8/8/8/8/4K3 w - a2")
      expect(f).to be_error
      expect(f.error).to eq t("fen.errors.invalid")
      expect(f.error_position).to eq "4k3/8/8/8/8/8/8/4K3 w - a^2"
    end

    it "half moves" do
      f = Fen.new("4k3/8/8/8/8/8/8/4K3 w - - -")
      expect(f).to be_error
      expect(f.error).to eq t("fen.errors.invalid")
      expect(f.error_position).to eq "4k3/8/8/8/8/8/8/4K3 w - - ^-"
    end

    it "space" do
      f = Fen.new("4k3/8/8/8/8/8/8/4K3 w - - 0x")
      expect(f).to be_error
      expect(f.error).to eq t("fen.errors.space")
      expect(f.error_position).to eq "4k3/8/8/8/8/8/8/4K3 w - - 0^x"
    end

    it "full moves" do
      f = Fen.new("4k3/8/8/8/8/8/8/4K3 w - - 0 0")
      expect(f).to be_error
      expect(f.error).to eq t("fen.errors.invalid")
      expect(f.error_position).to eq "4k3/8/8/8/8/8/8/4K3 w - - 0 ^0"
    end

    it "end" do
      f = Fen.new("4k3/8/8/8/8/8/8/4K3 w - - 0 1.")
      expect(f).to be_error
      expect(f.error).to eq t("fen.errors.invalid")
      expect(f.error_position).to eq "4k3/8/8/8/8/8/8/4K3 w - - 0 1^."
    end
  end
end
