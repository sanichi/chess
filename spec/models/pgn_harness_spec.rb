require 'rails_helper'

describe PgnHarness do
  describe "clean" do
    def pgn(headers, moves, nl: "\n", nlh: nil)
      nlh = nl if nlh.nil?
      "[]#{nlh}" * headers + nl + "m#{nl}" * moves
    end

    it "discards any games after the first" do
      expect(PgnHarness.clean(pgn(3, 8) + "\n" + pgn(7, 7))).to eq pgn(3,8)
    end

    it "discards leading and trailing space" do
      expect(PgnHarness.clean(" \r\n" + pgn(8, 3) + " \r\n")).to eq pgn(8, 3)
      expect(PgnHarness.clean("\n\r " + pgn(6, 1) + "\r\n ")).to eq pgn(6, 1)
    end

    it "converts line endings" do
      expect(PgnHarness.clean(pgn(9, 2, nl: "\r\n"))).to eq pgn(9, 2)
      expect(PgnHarness.clean(pgn(9, 4, nl: "\r"))).to eq pgn(9, 4)
    end

    it "canonicalises headers" do
      expect(PgnHarness.clean(pgn(9, 2, nlh: ""))).to eq pgn(9, 2)
      expect(PgnHarness.clean(pgn(10, 4, nlh: " "))).to eq pgn(10, 4)
      expect(PgnHarness.clean(pgn(7, 1, nlh: "\n\n"))).to eq pgn(7, 1)
    end

    it "removes timestamps" do
      expect(PgnHarness.clean("1.e4 {[%clk 0:04:59]} c5 {[%clk 0:05:03]} 1/2-1/2")).to eq "1.e4 c5 1/2-1/2"
    end

    it "does not modify an already clean file" do
      all_pgn_data.each do |pgn|
        expect(PgnHarness.clean(pgn)).to eq pgn
      end
    end
  end

  describe "complete" do
    def pgn(headers, moves, fen: Fen::INITIAL, result: "1/2-1/2")
      (fen ? "[FEN \"#{fen}\"]\n" : '') + "[]\n" * headers + "\n" + "m\n" * moves + (result ? "#{result}\n" : '')
    end

    it "no change if there's already a result" do
      expect(PgnHarness.complete(pgn(5, 3, result: "1-0"))).to eq pgn(5, 3, result: "1-0")
      expect(PgnHarness.complete(pgn(1, 2, result: "0-1"))).to eq pgn(1, 2, result: "0-1")
      expect(PgnHarness.complete(pgn(3, 9, result: "1/2-1/2"))).to eq pgn(3, 9, result: "1/2-1/2")
    end

    it "adds a dummy result if there's none" do
      expect(PgnHarness.complete(pgn(5, 5, result: nil))).to eq pgn(5, 5, result: "1/2-1/2")
    end

    it "no change if there's already a FEN" do
      expect(PgnHarness.complete(pgn(3, 8))).to eq pgn(3, 8)
    end

    it "adds the start FEN if there's no FEN at all" do
      expect(PgnHarness.complete(pgn(6, 3, fen: nil))).to eq pgn(6, 3)
    end
  end

  describe "test files" do
    it "consistency" do
      all_pgn_data.each do |pgn|
        h1 = PgnHarness.new(pgn)
        expect(h1).to be_okay
        h2 = PgnHarness.new(h1.pgn)
        expect(h2).to be_okay
        expect(h1.pgn).to eq h2.pgn
      end
    end

    it "need for FEN (or indeed any) tags" do
      all_pgn_data.each do |pgn|
        h = PgnHarness.new(pgn)
        expect(h).to be_okay
        if h.game.starting_position.to_fen.to_s == Fen::INITIAL
          expect(h.pgn.scan(/\[\w+/)).to eq []
        else
          expect(h.pgn.scan(/\[\w+/)).to eq ["[FEN"]
        end
      end
    end
  end
end
