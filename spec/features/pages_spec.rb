require 'rails_helper'

describe PagesController do
  let(:admin) { create(:user, admin: true) }
  let(:user)  { create(:user, admin: false) }

  context "admin" do
    before(:each) do
      login admin
    end

    context "home" do
      it "show" do
        expect(page).to have_title admin.name
      end
    end

    context "test" do
      it "show" do
        click_link t("pages.test.title")
        expect(page).to have_title t("pages.test.title")
      end
    end

    context "env" do
      it "show" do
        click_link t("pages.env.title")
        expect(page).to have_title t("pages.env.title")
      end
    end
  end

  context "user" do
    before(:each) do
      login user
    end

    context "home" do
      it "show" do
        expect(page).to have_title user.name
      end
    end

    context "test" do
      it "show" do
        expect(page).to_not have_css "a", text: t("pages.test.title")
        visit test_path
        expect_forbidden page
      end
    end

    context "env" do
      it "show" do
        expect(page).to_not have_css "a", text: t("pages.env.title")
        visit env_path
        expect_forbidden page
      end
    end
  end

  context "guest" do
    before(:each) do
      visit home_path
    end

    context "home" do
      it "show" do
        expect(page).to have_title t("pages.home.title")
      end
    end

    context "test" do
      it "show" do
        expect(page).to_not have_css "a", text: t("pages.test.title")
        visit test_path
        expect_forbidden page
      end
    end

    context "env" do
      it "show" do
        expect(page).to_not have_css "a", text: t("pages.env.title")
        visit env_path
        expect_forbidden page
      end
    end
  end
end
