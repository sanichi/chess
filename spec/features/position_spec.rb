require 'rails_helper'

describe Position do
  let(:admin) { create(:user, admin: true) }
  let(:user)  { create(:user, admin: false) }
  let(:data)  { build(:position) }
  let!(:p1)   { create(:position, user: admin) }
  let!(:p2)   { create(:position, user: user) }
  let(:xif)   { build(:position, pgn: bad_data("invalid_fen")) }
  let(:xnc)   { build(:position, pgn: bad_data("no_continuation")) }
  let(:xni)   { build(:position, pgn: bad_data("no_index")) }

  context "users" do
    before(:each) do
      login(user)
      click_link t("position.title")
    end

    context "create" do
      it "success" do
        click_link t("position.new")
        fill_in t("position.opening"), with: data.opening
        fill_in t("position.var"), with: data.var
        fill_in t("position.svar"), with: data.svar
        fill_in t("position.ssvar"), with: data.ssvar
        fill_in t("position.pgn"), with: data.pgn
        click_button t("save")

        not_expect_error(page)
        expect(page).to have_title data.opening
        expect(Position.count).to eq 3
        p = Position.order(:created_at).last
        expect(p.opening).to eq data.opening
        expect(p.var).to eq data.var
        expect(p.svar).to eq data.svar
        expect(p.ssvar).to eq data.ssvar
        expect(p.harness).to be_okay
        expect(p.game).to_not be_nil
      end

      context "failure" do
        it "no pgn" do
          click_link t("position.new")
          fill_in t("position.opening"), with: data.opening
          click_button t("save")

          expect_error(page, t("position.errors.parse"))
          expect(page).to have_title t("position.new")
          expect(Position.count).to eq 2
        end

        it "no index" do
          click_link t("position.new")
          fill_in t("position.opening"), with: xni.opening
          fill_in t("position.pgn"), with: xni.pgn
          click_button t("save")

          expect_error(page, t("position.errors.start"))
          expect(page).to have_title t("position.new")
          expect(Position.count).to eq 2
        end

        it "invalid fen" do
          click_link t("position.new")
          fill_in t("position.opening"), with: xif.opening
          fill_in t("position.pgn"), with: xif.pgn
          click_button t("save")

          expect_error(page, t("position.errors.fen"))
          expect(page).to have_title t("position.new")
          expect(Position.count).to eq 2
        end

        it "no continuation" do
          click_link t("position.new")
          fill_in t("position.opening"), with: xnc.opening
          fill_in t("position.pgn"), with: xnc.pgn
          click_button t("save")

          expect_error(page, t("position.errors.continuation"))
          expect(page).to have_title t("position.new")
          expect(Position.count).to eq 2
        end
      end
    end

    context "edit" do
      it "opening" do
        click_link p2.id.to_s
        click_link t("edit")

        expect(page).to have_title t("position.edit")

        fill_in t("position.opening"), with: data.opening
        fill_in t("position.var"), with: data.var
        fill_in t("position.svar"), with: data.svar
        fill_in t("position.ssvar"), with: data.ssvar
        click_button t("save")

        expect(page).to have_title data.opening
        expect(Position.count).to eq 2
        p = Position.order(:updated_at).last
        expect(p.opening).to eq data.opening
        expect(p.var).to eq data.var
        expect(p.svar).to eq data.svar
        expect(p.ssvar).to eq data.ssvar
      end

      it "pgn" do
        click_link p2.id.to_s
        click_link t("edit")

        expect(page).to have_title t("position.edit")

        fill_in t("position.pgn"), with: data.pgn
        click_button t("save")

        expect(page).to have_title p2.opening
        expect(Position.count).to eq 2
        p = Position.order(:updated_at).last
        expect(p.harness).to be_okay
        expect(p.game).to_not be_nil
      end

      it "forbidden" do
        click_link p1.id.to_s

        expect(page).to_not have_css "a", text: t("edit")

        visit edit_position_path(p1)

        expect_forbidden page
      end
    end

    context "copy" do
      it "success" do
        click_link p2.id.to_s
        click_link t("copy")

        expect(page).to have_title t("position.new")

        fill_in t("position.pgn"), with: p1.pgn
        click_button t("save")

        expect(page).to have_title p2.opening
        expect(Position.count).to eq 3
        p = Position.order(:updated_at).last
        expect(p.opening).to eq p2.opening
        expect(p.var).to eq p2.var
        expect(p.svar).to eq p2.svar
        expect(p.svar).to eq p2.svar
        expect(p.pgn).to eq p1.pgn
      end

      it "failure" do
        click_link p2.id.to_s
        click_link t("copy")

        expect(page).to have_title t("position.new")

        click_button t("save")

        expect(page).to have_title t("position.new")
        expect_error(page, t("position.errors.dup"))
        expect(Position.count).to eq 2
      end
    end

    context "delete" do
      it "success" do
        expect(Position.count).to eq 2

        click_link p2.id.to_s
        click_link t("edit")
        click_link t("delete")

        expect(page).to have_title t("position.title")
        expect(Position.count).to eq 1
      end
    end
  end

  context "guests" do
    before(:each) do
      visit root_path
    end

    it "view" do
      expect(page).to_not have_css "a", text: t("position.title")
      visit position_path(p1)
      expect_forbidden page
    end

    it "create" do
      visit new_position_path
      expect_forbidden page
    end

    it "edit" do
      visit edit_position_path(p1)
      expect_forbidden(page)
    end
  end
end
