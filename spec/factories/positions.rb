FactoryBot.define do
  factory :position do
    sequence(:pgn) { |i| next_pgn_data(i) }
    opening        { Faker::Lorem.words(number: 3).join(" ").truncate(Position::MAX_OPENING) }
    var            { [nil, Faker::Lorem.words(number: 3).join(" ").truncate(Position::MAX_OPENING)].sample }
    svar           { [nil, Faker::Lorem.words(number: 2).join(" ").truncate(Position::MAX_OPENING)].sample }
    ssvar          { [nil, Faker::Lorem.words(number: 1).join(" ").truncate(Position::MAX_OPENING)].sample }
    user
  end
end
