Rails.application.routes.draw do
  root to: "pages#home"

  %w{env help home test}.each { |p| get p => "pages##{p}" }
  get "sign_in" => "sessions#new"

  resources :notes
  resources :positions do
    get :copy, on: :member
    get :select, on: :collection
    get :retire, on: :collection
    get :review, on: :collection
  end
  resources :users

  resource :session, only: [:new, :create, :destroy]
end
