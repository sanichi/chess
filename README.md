# README

Templete of minimal Rails app with

* bootstrap
* cancancan
* capistrano
* capybara
* factory-bot
* faker
* haml
* jquery
* meta-tags
* rbenv
* rspec
* passenger
* postgressql

and other Mark Orr preferences.
