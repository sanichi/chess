class CreatePositions < ActiveRecord::Migration[6.1]
  def change
    create_table   :positions do |t|
      t.string     :fen, limit: 100
      t.string     :opening, limit: 25
      t.belongs_to :user

      t.timestamps
    end
  end
end
