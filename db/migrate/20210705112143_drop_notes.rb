class DropNotes < ActiveRecord::Migration[6.1]
  def up
    drop_table :notes
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
