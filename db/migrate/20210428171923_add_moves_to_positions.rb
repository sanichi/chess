class AddMovesToPositions < ActiveRecord::Migration[6.1]
  def change
    add_column :positions, :pgn, :text
  end
end
