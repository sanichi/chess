class AddVariationsToPositions < ActiveRecord::Migration[6.1]
  def change
    add_column :positions, :var, :string, limit: 25
    add_column :positions, :svar, :string, limit: 25
    add_column :positions, :ssvar, :string, limit: 25
  end
end
